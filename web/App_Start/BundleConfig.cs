﻿using System.Web;
using System.Web.Optimization;

namespace web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //#if DEBUG
            //            BundleTable.EnableOptimizations = true;
            //#endif
            BundleTable.EnableOptimizations = false;
            bundles.Add(new StyleBundle("~/bundles/bootstrap-css").Include(
                "~/Content/css/bootstrap.css",
                "~/Content/css/bootstrap-grid.css",
                "~/Content/css/bootstrap-grid.min.css",
                "~/Content/css/bootstrap-reboot.css",
                "~/Content/css/bootstrap-reboot.min.css",
                "~/Content/css/bootstrap.min"
                ));

            bundles.Add(new StyleBundle("~/bundles/AdminLTE-css").Include(
                "~/Content/css/AdminLTE.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jquery-js").Include(
                "~/Content/Lib/jquery-3.0.0.js",
                "~/Content/Lib/jquery-3.0.0.min.js",
                "~/Content/Lib/jquery-3.0.0.slim.js",
                "~/Content/Lib/jquery-3.0.0.slim.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryValidate-js").Include(
                "~/Content/Lib/jquery.validate.js",
                "~/Content/Lib/jquery.validate.min.js",
                "~/Content/Lib/jquery.validate-vsdoc.js",
                "~/Content/Lib/jquery.validate.unobtrusive.js",
                "~/Content/Lib/jquery.validate.unobtrusive.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-js").Include(
                "~/Content/Lib/bootstrap.js",
                "~/Content/Lib/bootstrap.min.js",
                "~/Content/Lib/bootstrap.bundle.js"));

            bundles.Add(new ScriptBundle("~/bundles/login-js").Include(
                "~/Scripts/login.js"));

            bundles.Add(new ScriptBundle("~/bundles/pruebaLayout-js").Include(
                "~/Scripts/pruebaLayout.js"));


        }
    }
}