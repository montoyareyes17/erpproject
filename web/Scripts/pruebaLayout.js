﻿
var modalAlert = {
    init: function (settings) {
            
    },

    createDiv : function (settings) {
        return $('<div>',settings);
    },
    
    prototype: function (settings) {
        var _modal = modalAlert.createDiv({ 'class': 'modal fade', 'tabindex': '-1', 'role': 'dialog' });
        var _modalDialog = modalAlert.createDiv({ 'class': 'modal-dialog', 'role': 'document' });
        var _modalContent = modalAlert.createDiv({ 'class': 'modal-content' });

        _modalDialog.append(_modalContent);
        _modal.append(_modalDialog);
        _modalContent.append(modalAlert.createHeader(settings));
        _modalContent.append(modalAlert.createBody(settings));
        _modalContent.append(modalAlert.createFooter(settings));
        return _modal;
    },

    createHeader: function (settings) {
        var _modalHeader = modalAlert.createDiv({ 'class': 'modal-header' });
        var title = $('<h4>').append(settings.title);
        _modalHeader.append(title);
        return _modalHeader;
    },

    createBody: function (settings) {
        var _modalBody = modalAlert.createDiv({ 'class': 'modal-body' });
        var text = $('<p>').append(settings.message);
        _modalBody.append(text);
        return _modalBody;
    },

    createFooter: function (settings) {
        var _modalFooter = modalAlert.createDiv({ 'class': 'modal-footer' });
        var _bootonClose = $('<input>', { 'class': 'btn btn-default', 'data-dismiss': 'modal' });
        if (settings.event != null) {
            var _bootonAceptar = $('<input>', { 'class': 'btn btn-default', 'data-dismiss': 'modal' });
            _bootonAceptar.on('click', settings.event);
            _modalFooter.append(_bootonAceptar);
        }
        
        return _modalFooter;
    },

    mostrar: function (settings) {
        console.log(modalAlert.prototype)
        $('#divEjemplo').append(modalAlert.prototype(settings))
        $('#divEjemplo').append('<div>')
        console.log(1)
        console.log($('#divEjemplo'))
    }

}

$.fn.modal_defaults={
    message: '',
    Title: 'Alert'
}